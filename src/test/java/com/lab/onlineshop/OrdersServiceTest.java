package com.lab.onlineshop;

import com.lab.onlineshop.model.Movie;
import com.lab.onlineshop.model.Order;
import com.lab.onlineshop.services.OrdersService;
import com.lab.onlineshop.services.exceptions.EmptyOrderException;
import com.lab.onlineshop.services.exceptions.LimitOrderException;
import com.lab.onlineshop.services.exceptions.OutOfStockException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

    @Mock
    EntityManager em;

    @Test(expected = OutOfStockException.class)
    public void whenOrderedMovieNotAvailable_placeOrderThrowsOutOfStockException() {
        Order order = new Order();
        Movie movie = new Movie();
        movie.setAmount(0);
        order.getMovies().add(movie);

        Mockito.when(em.find(Movie.class, movie.getId())).thenReturn(movie);
        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderedMoviesAndNotAvailable_placeOrderThrowsOutOfStockException() {
        Order order = new Order();
        Movie movie = new Movie();
        movie.setAmount(2);
        for(int i = 0; i < 3; i++) {
            order.getMovies().add(movie);
        }

        Mockito.when(em.find(Movie.class, movie.getId())).thenReturn(movie);
        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);
    }

    @Test
    public void whenOrderedManyTimesTheSameAvailableMovie_placeOrderDecreaseAmountByMoviesNumber() {
        Order order = new Order();
        Movie movie = new Movie();
        movie.setAmount(5);
        for(int i = 0; i < 4; i++) {
            order.getMovies().add(movie);
            Mockito.when(em.find(Movie.class, movie.getId())).thenReturn(movie);
        }

        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);

        assertEquals(1, (int)movie.getAmount());
        Mockito.verify(em, times(1)).persist(order);
    }

    @Test(expected = OutOfStockException.class)
    public void whenOrderedMoviesAndOneOfThemNotAvailable_placeOrderThrowsOutOfStockException() {
        Order order = new Order();
        Movie movie1 = new Movie();
        movie1.setAmount(1);
        Movie movie2 = new Movie();
        movie2.setAmount(0);

        order.getMovies().add(movie1);
        order.getMovies().add(movie2);

        Mockito.when(em.find(Movie.class, movie1.getId())).thenReturn(movie1);
        Mockito.when(em.find(Movie.class, movie2.getId())).thenReturn(movie2);
        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);
    }

    @Test
    public void whenOrderedMovieAvailable_placeOrderDecreasesAmountByOne() {
        Order order = new Order();
        Movie movie = new Movie();
        movie.setAmount(1);
        order.getMovies().add(movie);

        Mockito.when(em.find(Movie.class, movie.getId())).thenReturn(movie);
        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);

        assertEquals(0, (int)movie.getAmount());
        Mockito.verify(em, times(1)).persist(order);
    }

    @Test(expected = LimitOrderException.class)
    public void whenOrderedMoviesAboveLimit_placeOrderThrowsOutOfStockException() {
        Order order = new Order();
        Movie movie = new Movie();
        movie.setAmount(100);
        for(int i = 0;  i < 11; i++) {
            order.getMovies().add(movie);
            Mockito.when(em.find(Movie.class, movie.getId())).thenReturn(movie);
        }

        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);
    }

    @Test(expected = EmptyOrderException.class)
    public void whenNoMovieOrdered_placeOrderThrowsEmptyOrderException() {
        Order order = new Order();

        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(order);
    }

    @Test(expected = EmptyOrderException.class)
    public void whenOrderIsNull_placeOrderThrowsEmptyOrderException() {
        OrdersService ordersService = new OrdersService(em);

        ordersService.placeOrder(null);
    }
}