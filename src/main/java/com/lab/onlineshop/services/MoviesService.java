package com.lab.onlineshop.services;

import com.lab.onlineshop.model.Movie;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

@Service
public class MoviesService extends EntityService<Movie> {

    public MoviesService(EntityManager em) {
        super(em, Movie.class, Movie::getId);
    }

    public List<Movie> findAll() {
        return em.createNamedQuery(Movie.FIND_ALL, Movie.class).getResultList();
    }

    public Movie find(String title, String director) {
        try {
            Movie m = em.createQuery(
                    "SELECT a FROM Movie a " +
                            "WHERE TITLE='" + title + "' AND DIRECTOR='" + director + "'",
                    Movie.class).getSingleResult();
            return m;
        }
        catch(NoResultException ex) {
            return null;
        }
    }

    public List<Movie> findByTitle(String title) {
        Query query = em.createQuery(String.format("SELECT m FROM Movie m WHERE m.title LIKE '%%%s%%'", title), Movie.class);
        return query.getResultList();
    }
}