package com.lab.onlineshop.services;

import com.lab.onlineshop.model.Movie;
import com.lab.onlineshop.model.Order;
import com.lab.onlineshop.services.exceptions.EmptyOrderException;
import com.lab.onlineshop.services.exceptions.LimitOrderException;
import com.lab.onlineshop.services.exceptions.OutOfStockException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class OrdersService extends EntityService<Order> {

    static private final int orderLimit = 10;

    public OrdersService(EntityManager em) {
        super(em, Order.class, Order::getId);
    }

    public List<Order> findAll() {
        return em.createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }

    @Transactional
    public void placeOrder(Order order) {
        if(order == null || order.getMovies().size() == 0) {
            throw new EmptyOrderException();
        }
        if(order.getMovies().size() > orderLimit) {
            throw new LimitOrderException();
        }
        for (Movie movieStub : order.getMovies()) {
            Movie movie = em.find(Movie.class, movieStub.getId());
            if(movie.getAmount() < 1) {
                throw new OutOfStockException();
            }
            else {
                int newAmount = movie.getAmount() - 1;
                movie.setAmount(newAmount);
            }
        }

        save(order);
    }

}