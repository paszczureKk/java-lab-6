package com.lab.onlineshop.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.UUID;

@Entity
@Table(name = "movies")
@EqualsAndHashCode(of = "id")
@NamedQueries(value = {
        @NamedQuery(name = Movie.FIND_ALL, query = "SELECT m FROM Movie m")
})
public class Movie {

    public static final String FIND_ALL = "Movie.FIND_ALL";

    @Getter
    @Setter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @Setter
    String title;

    @Getter
    @Setter
    String director;

    @Getter
    @Setter
    Duration duration;

    @Getter
    @Setter
    BigDecimal price;

    @Getter
    @Setter
    Integer amount;

}