package com.lab.onlineshop.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
@EqualsAndHashCode(of = "id")
@NamedQueries(value = {
        @NamedQuery(name = Order.FIND_ALL, query = "SELECT o FROM Order o")
})
public class Order {
    public static final String FIND_ALL = "Order.FIND_ALL";

    @Getter
    @Id
    UUID id = UUID.randomUUID();

    @Getter
    @ManyToMany(cascade = {CascadeType.MERGE})
    List<Movie> movies = new ArrayList<>();

    @Getter
    @Temporal(TemporalType.TIMESTAMP)
    Date creationDate;

    @PrePersist
    public void prePersist() {
        this.creationDate = new Date();
    }
}