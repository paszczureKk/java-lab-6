package com.lab.onlineshop.controllers;

import com.lab.onlineshop.model.Order;
import com.lab.onlineshop.services.OrdersService;
import com.lab.onlineshop.services.exceptions.EmptyOrderException;
import com.lab.onlineshop.services.exceptions.LimitOrderException;
import com.lab.onlineshop.services.exceptions.OutOfStockException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class ShopController {
    final OrdersService ordersService;

    public ShopController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @GetMapping("/orders")
    public List<Order> listOrders() {
        return ordersService.findAll();
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable UUID id) {
            Order order = ordersService.find(id);
            return order == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(order);
    }

    @PostMapping("/orders")
    public ResponseEntity<Void> addOrder(@RequestBody Order order, UriComponentsBuilder uriComponentsBuilder) {
        try {
            ordersService.placeOrder(order);
            URI location = uriComponentsBuilder.path("/orders/{id}").buildAndExpand(order.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
        catch(OutOfStockException | EmptyOrderException | LimitOrderException ex) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    @GetMapping("/orders/product/{id}")
    public List<Order> listOrdersContainCertainMovie(@PathVariable UUID id) {
        return ordersService.findAll().stream().filter(m -> (m.getMovies().stream().anyMatch(n -> n.getId().equals(id)))).collect(Collectors.toList());
    }
}