package com.lab.onlineshop.controllers;


import com.lab.onlineshop.model.Movie;
import com.lab.onlineshop.services.MoviesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/movies")
public class MoviesController {

    final MoviesService moviesService;

    public MoviesController(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @GetMapping
    public List<Movie> listMovies() {
        return moviesService.findAll();
    }

    @GetMapping("/price/above/{price}")
    public List<Movie> getMoviesAbovePrice(@PathVariable BigDecimal price) {
        return moviesService.findAll().stream().filter(m -> m.getPrice().compareTo(price) > 0).collect(Collectors.toList());
    }

    @GetMapping("/price/below/{price}")
    public List<Movie> getMoviesBelowPrice(@PathVariable BigDecimal price) {
        return moviesService.findAll().stream().filter(m -> m.getPrice().compareTo(price) < 0).collect(Collectors.toList());
    }

    @GetMapping("/time/above/{minutes}")
    public List<Movie> getMoviesAboveDuration(@PathVariable long minutes) {
        return moviesService.findAll().stream().filter(m -> {
            long mDuration = m.getDuration().getSeconds() / 60;
            return (mDuration > minutes);
        }).collect(Collectors.toList());
    }

    @GetMapping("/time/below/{minutes}")
    public List<Movie> getMoviesBelowDuration(@PathVariable long minutes) {
        return moviesService.findAll().stream().filter(m -> {
            long mDuration = m.getDuration().getSeconds() / 60;
            return (mDuration < minutes);
        }).collect(Collectors.toList());
    }

    @PostMapping
    public ResponseEntity<Void> addMovie(@RequestBody Movie movie, UriComponentsBuilder uriBuilder) {
        if (moviesService.find(movie.getTitle(), movie.getDirector()) == null) {
            moviesService.save(movie);
            URI location = uriBuilder.path("/movies/{id}").buildAndExpand(movie.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
        else {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @GetMapping("/query/{titleFrag}")
    public List<Movie> getMoviesByTitleFragment(@PathVariable String titleFrag) {
        return moviesService.findByTitle(titleFrag);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable UUID id) {
        Movie movie = moviesService.find(id);
        return movie != null ? ResponseEntity.ok(movie) : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateMovie(@RequestBody Movie movie, @PathVariable UUID id) {
        if (moviesService.find(id) != null) {
            movie.setId(id);
            moviesService.save(movie);
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/amount/{number}")
    public List<Movie> getMoviesWithLowerAmount(@PathVariable Integer number) {
        return moviesService.findAll().stream().filter(m -> m.getAmount() < number).collect(Collectors.toList());
    }
}